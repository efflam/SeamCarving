# SeamCarving 

Ceci est une implémentation en Java de l'algorithme de redimensionnement d'image s'appelant ["Seam Carving"](https://dl.acm.org/doi/10.1145/1275808.1276390).
Elle a été réalisée en binôme dans le cadre de l'unité d'Algorithmique à l'ESIEE Paris.

## Implémentation

Le programme n'implémente que la réduction de taille d'une image, bien que le Seam Carving permette théoriquement l'augmentation de taille.
Tout d'abord, on choisit une fonction énergie, qui détermine l'"énergie" d'un pixel en fonction de ses voisins. Nous avons choisi la méthode du ["double gradient"](https://coursera.cs.princeton.edu/algs4/assignments/seam/specification.php).
Le Seam Carving fonctionne en retirant des chemins (seams) avec une énergie minimale.
Le programme va ainsi retirer ligne par ligne et colonne par colonne les seams les plus faibles jusqu'à arriver à la taille voulue.
Le rapport de S. Avidan et A. Shamir décrit un ordre optimal de suppression des seams passant par le calcul d'une "carte de transport" T. 
En revanche, nous n'avons pas utilisé cette méthode, puisque c'est un procédé qui rajoute du temps de calcul, et qui produit des résultats similaires à la méthode que nous avons choisi, qui est d'alterner entre suppression de seams horizontaux et verticaux.  

Tant que la taille voulue n'est pas atteinte, on utilise une approche dynamique en calculant un tableau M\[L\]\[H\] correspondant à la somme minimale des énergies de pixel jusqu'à la case voulue. 
Ce tableau peut avoir 2 sens, horizontal ou vertical, et est utilisé selon si on veut supprimer une ligne ou une colonne.
Enfin, on détermine le seam le plus faible en utilisant la dernière ligne/colonne du tableau et en déterminant le chemin en remontant le tableau.
On supprime ce seam, et on répète ces opérations.


## Compilation et exécution

Java et JDK doivent être installés sur la machine.
Le programme se compile en exécutant la commande `javac SeamCarving.java`
Ensuite, le programme se lance en tapant `java SeamCarving img rx ry` où `img` désigne le chemin d'une image (au format supporté par [ImageIO](https://docs.oracle.com/javase/7/docs/api/javax/imageio/package-summary.html)), 
et `rx` et `ry` respectivement les pourcentages de réduction de taille en largeur et en hauteur.
Lorque le programme est en cours d'exécution, une barre de progression s'affiche.
Une fois le programme terminé, il produira un fichier `img_resized_rx_ry.png`. 
