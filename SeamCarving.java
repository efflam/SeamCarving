import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;


/**
 * Permet d'utiliser l'algorithme de remise à l'échelle "Seam Carving" sur une image
 * @author Jules (dkj3)
 * @author Efflam (efflam)
 * @version 1.0
 * @since 2020-06-15
*/
public class SeamCarving {

	/**
	* Calcule l'énergie d'un pixel d'une image donnée en utilisant la méthode "dual-gradient"
	* @param image L'image source du pixel
	* @param x     La coordonnée en X du pixel dont on veut calculer l'énergie
	* @param y     La coordonnée en Y du pixel dont on veut calculer l'énergie
	* @return      Un entier (int) entre 0 et 1081 correspondant à l'énergie du pixel
	*/
	public static int calculerEnergiePixel(BufferedImage image, int x, int y) {
		//Fonction énergie dite "dual-gradient", décrite en détail ici:
		//https://coursera.cs.princeton.edu/algs4/assignments/seam/specification.php

		//Si le pixel est au bord de l'image, on lui donne une valeur maximale afin de conserver l'information au bord
		if(x == 0 || x == image.getWidth() - 1 || y == 0 || y == image.getHeight() - 1)
			return 1081; //1081 étant la valeur maximale possible

		//On stocke les couleurs des pixels adjacents
		Color PXplus  = new Color(image.getRGB(x + 1, y));
		Color PXmoins = new Color(image.getRGB(x - 1, y));
		Color PYplus  = new Color(image.getRGB(x, y + 1));
	 	Color PYmoins = new Color(image.getRGB(x, y - 1));

	 	//On calcule la différence entre les rouges, verts et bleus des pixels adjacents en X
	 	int Rx = PXplus.getRed()   - PXmoins.getRed();
	 	int Gx = PXplus.getGreen() - PXmoins.getGreen();
	 	int Bx = PXplus.getBlue()  - PXmoins.getBlue();

	 	//On calcule la différence entre les rouges, verts et bleus des pixels adjacents en Y
	 	int Ry = PYplus.getRed()   - PYmoins.getRed();
	 	int Gy = PYplus.getGreen() - PYmoins.getGreen();
	 	int By = PYplus.getBlue()  - PYmoins.getBlue();

	 	//On calcule la somme des différences quadratiques en X et en Y
	 	int deltaX = (int)(Math.pow(Rx, 2) + Math.pow(Gx, 2) + Math.pow(Bx, 2));
	 	int deltaY = (int)(Math.pow(Ry, 2) + Math.pow(Gy, 2) + Math.pow(By, 2));

	 	//On renvoie la racine carrée de la somme de ΔX et ΔY
		return (int)(Math.sqrt(deltaX + deltaY));
	}

	/**
	* Calcule le tableau M où la case M[X][Y] correspond à la somme des énergies minimum
	* en allant de la colonne X=0 jusqu'à la case M[X][Y], donc en sens horizontal
	* @param image L'image pour laquelle on souhaite calculer M
	* @return      Le tableau M en sens horizontal
	*/
	public static int[][] calculerMHorizontal(BufferedImage image) {
		//On initialise le nombre de colonnes et de lignes
		int C = image.getWidth(), L = image.getHeight();
		//On crée le tableau M
		int[][] M = new int[C][L];
		for(int x = 0; x < C; x++) {
			for(int y = 0; y < L; y++) {
				//On calcule la valeur de l'énergie du pixel actuel
				M[x][y] = calculerEnergiePixel(image, x, y);
				//Si on est sur la première colonne, il n'y a pas de valeurs précédentes à additionner
				if(x == 0) continue;
				//On détermine la valeur minimale entre les 3 pixels adjecents de la colonne précédente
				int plusPetiteValeur = M[x-1][y];
				if(y > 0)
					if(M[x-1][y-1] < plusPetiteValeur)
						plusPetiteValeur = M[x-1][y-1];
				if(y < L-1)
					if(M[x-1][y+1] < plusPetiteValeur)
						plusPetiteValeur = M[x-1][y+1];
				//On l'additionne à l'énergie du pixel actuel
				M[x][y] += plusPetiteValeur;
			}
		}
		return M;
	}

	/**
	* Calcule le tableau M où la case M[X][Y] correspond à la somme des énergies minimum
	* en allant de la ligne Y=0 jusqu'à la case M[X][Y], donc en sens vertical
	* @param image L'image pour laquelle on souhaite calculer M
	* @return      Le tableau M en sens vertical
	*/
	public static int[][] calculerMVertical(BufferedImage image) {
		//On initialise le nombre de colonnes et de lignes
		int C = image.getWidth(), L = image.getHeight();
		//On crée le tableau M
		int[][] M = new int[C][L];
		for(int y = 0; y < L; y++) {
			for(int x = 0; x < C; x++) {
				//On calcule la valeur de l'énergie du pixel actuel
				M[x][y] = calculerEnergiePixel(image, x, y);
				//Si on est sur la première ligne, il n'y a pas de valeurs précédentes à additionner
				if(y == 0) continue;
				//On détermine la valeur minimale entre les 3 pixels adjecents de la ligne précédente
				int plusPetiteValeur = M[x][y-1];
				if(x > 0)
					if(M[x-1][y-1] < plusPetiteValeur)
						plusPetiteValeur = M[x-1][y-1];
				if(x < C-1)
					if(M[x+1][y-1] < plusPetiteValeur)
						plusPetiteValeur = M[x+1][y-1];
				//On l'additionne à l'énergie du pixel actuel
				M[x][y] += plusPetiteValeur;
			}
		}
		return M;
	}

	/**
	* Calcule le seam (chemin) à l'énergie la plus faible dans le sens horizontal (de gauche à droite)
	* @param M Tableau correspondant à la somme horizontale des énergies minimum
	* @return  Le seam horizontal le plus faible
	*/
	public static int[] calculerPlusFaibleSeamHorizontal(int[][] M) {
		//On initialise le nombre de colonnes et de lignes
		int C = M.length, L = M[0].length;
		// On crée un seam d'une taille égale au nombre de colonnes
		int[] seam = new int[C];
		//On détermine quel chemin final à l'énergie minimale
		int minIndex = 0;
		for(int i = 1; i < L; i++)
			if(M[C-1][i] < M[C-1][minIndex])
				minIndex = i;
		//On initialise la case finale du seam à l'index trouvé
		seam[C-1] = minIndex;
		//On détermine le seam en partant de la fin
		for(int i = C-1; i > 0; i--) {
			//On trouve la somme à l'énergie la plus faible entre les 3 cases adjecentes de la colonne précédente
			minIndex = seam[i];
			if(seam[i] > 0)
				if(M[i-1][seam[i]-1] < M[i-1][minIndex])
					minIndex = seam[i]-1;
			if(seam[i] < L-1)
				if(M[i-1][seam[i]+1] < M[i-1][minIndex])
					minIndex = seam[i]+1;
			//On l'ajoute ensuite sa case au seam
			seam[i-1] = minIndex;
		}

		return seam;
	}

	/**
	* Calcule le seam (chemin) à l'énergie la plus faible dans le sens vertical (du haut vers le bas)
	* @param M Tableau correspondant à la somme verticale des énergies minimum
	* @return  Le seam vertical le plus faible
	*/
	public static int[] calculerPlusFaibleSeamVertical(int[][] M) {
		//On initialise le nombre de colonnes et de lignes
		int C = M.length, L = M[0].length;
		// On crée un seam d'une taille égale au nombre de lignes
		int[] seam = new int[L];
		//On détermine quel chemin final à l'énergie minimale
		int minIndex = 0;
		for(int i = 1; i < C; i++)
			if(M[i][L-1] < M[minIndex][L-1])
				minIndex = i;
		//On initialise la case finale du seam à l'index trouvé
		seam[L-1] = minIndex;
		//On détermine le seam en partant de la fin
		for(int i = L-1; i > 0; i--) {
			//On trouve la somme à l'énergie la plus faible entre les 3 cases adjecentes de la ligne précédente
			minIndex = seam[i];
			if(seam[i] > 0)
				if(M[seam[i]-1][i-1] < M[minIndex][i-1])
					minIndex = seam[i]-1;
			if(seam[i] < C-1)
				if(M[seam[i]+1][i-1] < M[minIndex][i-1])
					minIndex = seam[i]+1;
			//On l'ajoute ensuite sa case au seam
			seam[i-1] = minIndex;
		}

		return seam;
	}

	/**
	* Crée une image avec une ligne en moins, en retirant un seam horizontal
	* @param image L'image source à traiter
	* @param seam  Le seam à retirer
	* @return      Une nouvelle image sans le seam
	*/
	public static BufferedImage retirerSeamHorizontal(BufferedImage image, int[] seam) {
		//On initialise le nombre de colonnes et de lignes
		int C = image.getWidth(), L = image.getHeight();
		//On crée l'image de sortie avec une ligne en moins
		BufferedImage nouvelleImage = new BufferedImage(C, L-1, BufferedImage.TYPE_INT_RGB);
		for(int x = 0; x < C; x++) {
			//La valeur qui permet de décaler d'un pixel une fois le seam dépassé
			int decalage = 0;
			for(int y = 0; y < L; y++) {
				//Si on dépasse le seam, on décale d'un pixel vers le haut sur l'image de destination
				if(seam[x] == y) {
					decalage = -1;
					continue;
				}
				//On affecte a l'image de destination le pixel de l'image source
				nouvelleImage.setRGB(x, y+decalage, image.getRGB(x, y));
			}
		}
		return nouvelleImage;
	}

	/**
	* Crée une image avec une colonne en moins, en retirant un seam vertical
	* @param image L'image source à traiter
	* @param seam  Le seam à retirer
	* @return      Une nouvelle image sans le seam
	*/
	public static BufferedImage retirerSeamVertical(BufferedImage image, int[] seam) {
		//On initialise le nombre de colonnes et de lignes
		int C = image.getWidth(), L = image.getHeight();
		//On crée l'image de sortie avec une colonne en moins
		BufferedImage nouvelleImage = new BufferedImage(C-1, L, BufferedImage.TYPE_INT_RGB);

		for(int y = 0; y < L; y++) {
			//La valeur qui permet de décaler d'un pixel une fois le seam dépassé
			int decalage = 0;
			for(int x = 0; x < C; x++) {
				//Si on dépasse le seam, on décale d'un pixel vers la gauche sur l'image de destination
				if(seam[y] == x) {
					decalage = -1;
					continue;
				}
				//On affecte a l'image de destination le pixel de l'image source
				nouvelleImage.setRGB(x+decalage, y, image.getRGB(x, y));
			}
		}
		return nouvelleImage;
	}

	/**
	* Renvoie le nom de l'image de sortie à partir des pourcentages
	* @param s  Nom de l'image d'origine
	* @param rx Pourcentage de recadrement des colonnes
	* @param ry Pourcentage de recadrement des lignes
	* @return   Nom de l'image de sortie
	*/
	public static String nomSortie(String s, int rx, int ry) {
		int lio = s.lastIndexOf(".");
		return s.substring(0, lio) + "_resized_" + rx + "_" + ry + ".png"; //La sortie est toujours en ".png"
	}

	/**
	* Affiche une barre de progression dans le terminal
	* @param seamsTotal   Le nombre total de seams à traiter
	* @param seamsTraites Le nombre actuel de seams qui ont été traités
	*/
	public static void afficherProgression(int seamsTotal, int seamsTraites) {
		//Détermine la taille de la barre de progression
		int nombreEchelons = 50;
		//Calcul du pourcentage de seams traités
		int pourcentage = (int)(100.0 * seamsTraites / (double)seamsTotal);
		//Calcul du nombre de caractères à afficher
		int echelonsAffiches = (int)((double)nombreEchelons * (double)pourcentage / 100.0);
		//Affichage de la barre
		System.out.print("Recadrage [");
		for(int i = 0; i < echelonsAffiches-1; i++)
			System.out.print("=");
		if(pourcentage != 0)
			System.out.print(">");
		for(int i = 0; i < nombreEchelons - echelonsAffiches; i++)
			System.out.print(" ");
		System.out.print("] " + pourcentage + "% (" + seamsTraites + "/" + seamsTotal + ")\r");
	}

	/**
	* Réalise un seam carving sur une image
	* @param nomFichier Chemin de l'image à traiter
	* @param rx         Pourcentage de recadrement des colonnes
	* @param ry         Pourcentage de recadrement des lignes
	*/
	public static void seamCarving(String nomFichier, int rx, int ry) {
		//Bloc try pour générer une erreur si le fichier n'existe pas
		try {
			//On ouvre le fichier en entrée
			File fichierEntree = new File(nomFichier);
			//On ouvre le fichier de sortie
			File fichierSortie = new File(nomSortie(nomFichier, rx, ry));
			//On crée un objet image à partir du fichier d'entrée
			BufferedImage image = ImageIO.read(fichierEntree);
			//On initialise le nombre de colonnes et de lignes
			int C = image.getWidth(), L = image.getHeight();
			//On calcule la nouvelle taille de l'image en fonction des pourcentages
			int nouveauC = (int)(C*((double)rx/100.0)), nouveauL = (int)(L*((double)ry/100.0));
			//On ajoute une taille minimum d'1x1 pour ne pas générer d'erreur
			if(nouveauC <= 0)
				nouveauC = 1;
			if(nouveauL <= 0)
				nouveauL = 1;

			//On calcule le nombre total de seams à traiter
			int seamsTotal = C - nouveauC + L - nouveauL;
			int seamsTraites = 0;
			//Tant que l'image n'a pas atteint sa taille finale, on continue le seam carving
			while(image.getWidth() > nouveauC || image.getHeight() > nouveauL) {
				//Afficher la barre de progression
				afficherProgression(seamsTotal, seamsTraites);
				
				//L'ordre de traitement des seams est une alternance entre seam horizontal et vertical
				//La raison est que calculer la "carte de transport" T qui détermine l'ordre optimal
				//des seams prend un temps important de calcul, pour des résultats qui sont au final
				//très proche d'une simple alternance, c'est pourquoi nous avons utilisé cette méthode.

				//Si le nombre de lignes n'est pas celui attendu, alors on retire un seam horizontal
				if(image.getHeight() > nouveauL) {
					//On calcule M
					int[][] M = calculerMHorizontal(image);
					//On calcule le seam horizontal le plus faible de M
					int[] seam = calculerPlusFaibleSeamHorizontal(M);
					//On retire ce seam de l'image
					image = retirerSeamHorizontal(image, seam);
					//On incrémente le nombre de seams traités
					seamsTraites++;
				}

				//Si le nombre de colonnes n'est pas celui attendu, alors on retire un seam vertical
				if(image.getWidth() > nouveauC) {
					//On calcule M
					int[][] M = calculerMVertical(image);
					//On calcule le seam vertical le plus faible de M
					int[] seam = calculerPlusFaibleSeamVertical(M);
					//On retire ce seam de l'image
					image = retirerSeamVertical(image, seam);
					//On incrémente le nombre de seams traités
					seamsTraites++;
				}
			}
			//On affiche la progression finale
			afficherProgression(seamsTotal, seamsTraites);
			//On écrit l'image finale dans le dossier de sortie
			ImageIO.write(image, "png", fichierSortie);

		}
		//Cas où le fichier passé en paramètres n'existe pas
		catch(IOException e) {
			System.out.println("Le fichier n'existe pas!");
		}
	}

	/**
	* Réalise un seam carving sur une image passée en paramètre.
	* @param args Les paramètres du programme sont:
	* - Le chemin d'une image d'entrée
	* - Le pourcentage de réduction de colonnes
	* - Le pourcentage de réduction de lignes
	*/
	public static void main(String[] args) {
		//Le programme prend 3 arguments en entrée
		if(args.length < 3) {
			System.out.println("Utilisation: SeamCarving <fichier> <ratiox> <ratioy>");
			return;
		}
		//On récupère le nom du fichier
		String nomFichier = args[0];
		int rx, ry;
		try {
			//On s'assure que les paramètres soient bien des entiers entre 1 et 100 (seule la réduction est implémentée)
			rx = Integer.parseInt(args[1]);
			ry = Integer.parseInt(args[2]);
			if(rx <= 0 || rx > 100 || ry <= 0 || ry > 100)
				throw new NumberFormatException("Entiers en dehors de l'intervalle [1;100]");
		}
		//En cas d'erreur de conversion en int ou si les entiers ne sont pas entre 1 et 100
		catch(NumberFormatException e) {
			System.out.println("Merci d'entrer des entiers entre 1 et 100 pour les ratios");
			return;
		}

		//On réalise le Seam Carving sur l'image
		seamCarving(nomFichier, rx, ry);

	}
}

//<コ:彡